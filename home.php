<?php
	session_start();
	if($_SESSION['isLogin'] != true || $_SESSION['jam_selesai']==date("Y-m-d H:i:s"))
	{
		header("Location: form_login.php?message=nologin");
	}
	echo "Selamat datang, ",strtoupper($_SESSION['uname'])," login pada: ",$_SESSION['jam_mulai'];
	echo "<br>";
?>
	<a href="logout.php">Logout</a>

	<!DOCTYPE html>
	<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Document</title>
	</head>
	<body>
	<ul>
    <form action="simpandata.php" method="post">
        <!-- for & id harus sama agar berhubungan antara tulisan Username dengan input text -->
        <!-- nama lengkap -->
        <li>
            <label for="nama">Nama Lengkap : </label>
            <input type="text" name="nama" id="nama">
        </li>
        <!-- Username -->
        <li>
            <label for="username">Username : <?= $_SESSION['uname'] ?> </label>
        </li>
        <!-- Alamat -->
        <li>
            <label for="alamat">Alamat : </label>
            <textarea name="alamat" id="alamat"> </textarea>
        </li>
        <!-- Jenis Kelamin -->
        <li>
            <h4>Jenis Kelamin :</h4>
            <input type="radio" name="gender" id="gender" value="Laki - laki">
            <label for="gender">Pria</label>
            <input type="radio" name="gender" id="gender" value="Perempuan">
            <label for="gender">Wanita</label>
        </li>
        <!--  Hobi-->
        <li>
            <h4>Hobi :</h4>
            <label class="container">Bersepeda
                <input type="checkbox" name="hobi[]" value="Bersepeda">
                <span class="checkmark"></span>
            </label>
            <label class="container">Bermain Game
                <input type="checkbox" name="hobi[]" value="Bermain Game">
                <span class="checkmark"></span>
            </label>
            <label class="container">Membaca Buku
                <input type="checkbox" name="hobi[]" value="Membaca Buku">
                <span class="checkmark"></span>
            </label>
            <label class="container">Sunmori
                <input type="checkbox" name="hobi[]" value="Sunmori">
                <span class="checkmark"></span>
            </label>
            <label class="container">Menonton Film
                <input type="checkbox" name="hobi[]" value="Menonton Film">
                <span class="checkmark"></span>
            </label>
        </li>
        <!-- Pekerjaaan -->
        <li>
            <label for="pekerjaan">Pilih Posisi Pekerjaan :</label>
            <select name="pekerjaan" id="cars">
                <option value="IT Manager">IT Manager</option>
                <option value="Database Engineer">Database Engineer</option>
                <option value="Backend Engineer">Backend Engineer</option>
                <option value="Frontend Egineer">Frontend Egineer</option>
                <option value="UI / UX Desain">UI / UX Design</option>
                <option value="IT Consultant">IT Consultant</option>
            </select>
        </li>
        <!-- Submit -->
        <li>
            <button type="submit" name="submit" value="Login">Simpan</button>
        </li>
    </form>
    </ul> 
		
	</body>
	</html>
	<!-- 
		tugas 2:
		tambahkan form yang method-nya post ke halaman simpandata.php
		nama lengkap: text
		username : ambil dari session
		alamat: textarea
		jenis kelamin: radio, walaupun pilihannya 2, name tetap 1
		hobi: checkbox -> diisi terserah minimal 3 pilihan, pilihannya 3, name juga 3
		pekerjaan: select -> diisi terserah minimal 3 pilihan, sama kayak radio

		tombol simpan


		kalo sudah klik simpan, tampilkan hasil inputannya
	-->
